import React,{Component} from 'react';
import { Input,Button} from 'antd';
import 'antd/dist/antd.css';
import './input.css';
class Inputfields extends Component{
     constructor(){
         super();
         
          this.state = {
             data:'',
             final:''
         }
     }
    handle(event){
        this.setState({
             data:event.target.value
        })
      
    }
    handleClick(e) {

     this.setState({
         final:this.state.data
     })

        
    }
    render(){
        return(
            <div id="sizebox">
        InputFields: <Input id="numb" onChange={this.handle.bind(this)}/><br></br><br></br>
           <Button type="primary" onClick={this.handleClick.bind(this)} >Submit</Button><br></br><br></br>
        <h3>{this.state.final}</h3>
        
         </div>
        )
    }
}
export default Inputfields;